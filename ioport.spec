# ioport.spec.  Generated from ioport.spec.in by configure.

Summary:     Access I/O ports
Name:        ioport
Version:     1.2
Release:     1%{?dist}
License:     GPLv2+
Group:       Development/Tools
URL:         http://et.redhat.com/~rjones/ioport/
Source0:     http://et.redhat.com/~rjones/ioport/files/%{name}-%{version}.tar.gz
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root
ExclusiveArch: %{ix86} x86_64


%description
These commands enable command line and script access directly to I/O
ports on PC hardware.

The inb, inw and inl commands perform an input (read) operation on the
given I/O port, and print the result.

The outb, outw and outl commands perform an output (write) operation
to the given I/O port, sending the given data.  Note that the order of
the parameters is ADDRESS DATA.

The size of the operation is selected according to the suffix, with
'b' meaning byte, 'w' meaning word (16 bits) and 'l' meaning long
(32 bits).


%prep
%setup -q


%build
%configure
make


%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING README
%{_bindir}/inb
%{_bindir}/inw
%{_bindir}/inl
%{_bindir}/outb
%{_bindir}/outw
%{_bindir}/outl
%{_mandir}/man1/inb.1*
%{_mandir}/man1/inw.1*
%{_mandir}/man1/inl.1*
%{_mandir}/man1/outb.1*
%{_mandir}/man1/outw.1*
%{_mandir}/man1/outl.1*


%changelog
* Mon Mar 16 2009 Richard Jones <rjones@redhat.com> - 1.1-1
- Only offer to build on x86 and x86-64 architectures.

* Mon Mar  9 2009 Richard Jones <rjones@redhat.com> - 1.0-1
- Initial build.
